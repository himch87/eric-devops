package com.ericsson.lizard.basket.dao;

import java.util.Collection;

import com.ericsson.lizard.basket.model.Basket;


public interface BasketDao {

    public Basket getBasketById(int id);
    public Collection<Basket> getBaskets();
    
}
