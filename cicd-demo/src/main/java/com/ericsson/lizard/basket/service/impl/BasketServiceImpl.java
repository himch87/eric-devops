package com.ericsson.lizard.basket.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ericsson.lizard.basket.dao.BasketDao;
import com.ericsson.lizard.basket.model.Basket;
import com.ericsson.lizard.basket.service.BasketService;

@Service
public class BasketServiceImpl implements BasketService {

    @Autowired
    private BasketDao basketDao;
    
    @Override
    public Basket getBasketById(int id) {
        return basketDao.getBasketById(id);
    }
    
    @Override
    public Collection<Basket> getBaskets() {
        return basketDao.getBaskets();
    }

}
