package com.ericsson.lizard.basket.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ericsson.lizard.basket.model.Basket;
import com.ericsson.lizard.basket.service.BasketService;

@RestController
@RequestMapping("/baskets")
public class BasketController {

    @Autowired
    private BasketService basketService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Basket> getBaskets(){
        return basketService.getBaskets();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Basket getBasketById(@PathVariable("id") int id){
        return basketService.getBasketById(id);
    }

}
