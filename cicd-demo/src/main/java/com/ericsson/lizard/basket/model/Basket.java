package com.ericsson.lizard.basket.model;

public class Basket {

    private int id;
    private String creationDate;
    private int price;
    
    public Basket(int id, String creationDate, int price){
        this.id =  id;
        this.creationDate = creationDate;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public int getPrice() {
        return price;
    }
    
    
}
