package com.ericsson.lizard.basket.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/env")
public class BuildController {

	@RequestMapping(method=RequestMethod.GET)
	public Map<String, String> getBuildNumber(){
		
		Map<String, String> env = new HashMap<String, String>();
		Manifest manifest = null;
        
        InputStream inputStream = BuildController.class.getResourceAsStream("/META-INF/MANIFEST.MF");
		try {
			manifest = new Manifest(inputStream);
		} catch (IOException e) {
			//e.printStackTrace(); 
			System.out.println("BuildController.getBuildNumber error: " + e.getMessage());
		}
        
		if(manifest != null){
	        Attributes attributes = manifest.getMainAttributes();
	        env.put("version_number", attributes.getValue("Implementation-Version"));			
		}else {
			env.put("version_number", "0");
		}

        
        String hostname = System.getenv("COMPUTERNAME");
        if(hostname == null || "".equals(hostname.trim())){
        	hostname = System.getenv("HOSTNAME");
        }
        env.put("host_name", hostname);
        
        return env;
		
	}

}
