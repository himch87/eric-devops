package com.ericsson.lizard.basket.service;

import java.util.Collection;

import com.ericsson.lizard.basket.model.Basket;


public interface BasketService {

    public Basket getBasketById(int id);
    public Collection<Basket> getBaskets();
    
}
