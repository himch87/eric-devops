package com.ericsson.lizard.basket.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ericsson.lizard.basket.dao.BasketDao;
import com.ericsson.lizard.basket.model.Basket;

@Repository
public class BasketDaoImpl implements BasketDao {

    private static Map<Integer, Basket> basketMap;
    
    static{
        
        basketMap = new HashMap<Integer, Basket>();
      
        basketMap.put(1, new Basket(1, "2016-02-28", 100));
        basketMap.put(2, new Basket(2, "2016-12-15", 200));
        basketMap.put(3, new Basket(3, "2017-01-20", 300));
        basketMap.put(4, new Basket(4, "2018-02-26:01:04PM", 400));

    }
    
    @Override
    public Basket getBasketById(int id) {
        return basketMap.get(id);
    }

    @Override
    public Collection<Basket> getBaskets() {
        return basketMap.values();
    }
    
}
