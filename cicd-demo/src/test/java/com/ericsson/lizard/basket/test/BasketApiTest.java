package com.ericsson.lizard.basket.test;

import java.util.Collection;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.ModelAndView;

import com.ericsson.lizard.basket.HomeController;
import com.ericsson.lizard.basket.application.Application;
import com.ericsson.lizard.basket.controller.BasketController;
import com.ericsson.lizard.basket.controller.BuildController;
import com.ericsson.lizard.basket.model.Basket;

@RunWith(SpringRunner.class)

@SpringBootTest(classes = Application.class)

@WebAppConfiguration
public class BasketApiTest {

	@Autowired
	BasketController basketController;
	
	@Autowired
	HomeController homeController;
	
	@Test
	public void testBuildNumber() {
		BuildController bc = new BuildController();
		Map<String,String> bnMap = bc.getBuildNumber();
		Assert.assertNull(bnMap.get("Build Version Number"));

	}
	
	@Test
	public void testBaskets() {

		Collection<Basket> baskets = basketController.getBaskets();
		Assert.assertNotNull(baskets);
	}
	
	@Test
	public void testBasketsById() {

		Basket basket = basketController.getBasketById(1);
		Assert.assertNotNull(basket);
		Assert.assertNotNull(basket.getId());
		Assert.assertNotNull(basket.getCreationDate());
		Assert.assertNotNull(basket.getPrice());
	}
	
	@Test
	public void testHomeController() {

		ModelAndView mv = homeController.home(null, null);
		Assert.assertNotNull(mv);
	}
	

}
